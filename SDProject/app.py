from flask import Flask, render_template, url_for, redirect, request, jsonify
from flask_restful import Resource, Api
from flasgger import Swagger
import mysql.connector
from typing import List, Dict, Tuple

# Initialize Flask application
app = Flask(__name__)
api = Api(app)
swagger = Swagger(app)

# Connect to MySQL database
mydb = mysql.connector.connect(
    host="mysql",
    user="root",
    password="Darshana@1310",
    database="FlashCard"
)
crsr = mydb.cursor()

# Sample data to simulate created sets and flashcards
topic: str = ""
sql_command: str = "SELECT Topic FROM Questions GROUP BY Topic"
crsr.execute(sql_command)
myresult: List[Tuple[str]] = crsr.fetchall()
flashcards: List[Dict[str, str]] = []


@app.route('/')
def main_page() -> str:
    """
    Render the main page.

    :return: Rendered HTML template for the main page.
    """
    return render_template('main_page.html')


@app.route('/process')
def process_page() -> str:
    """
    Render the process page.

    :return: Rendered HTML template for the process page.
    """
    return render_template('process_page.html')


@app.route('/create_set', methods=['GET', 'POST'])
def create_set() -> str:
    """
    Create a new set of flashcards.
    If the set name does not exist, it redirects to the flashcard creation page.

    :return: Redirect to flashcard creation page or render create set template.
    """
    if request.method == 'POST':
        set_name: str = request.form['set_name']
        global topic
        if set_name not in myresult:
            topic = set_name
        return redirect(url_for('create_fc', topic=topic))
    return render_template('create_set.html')


@app.route('/create_fc', methods=['GET', 'POST'])
def create_fc() -> str:
    """
    Create a new flashcard in the current set.
    POST: Adds a new flashcard to the database and the session's flashcard list.

    :return: Render flashcard creation template.
    """
    global topic
    if request.method == 'POST':
        topic = request.form['topic']
        question: str = request.form['question']
        answer: str = request.form['answer']
        hint: str = request.form.get('hint', '')  # Hint is optional
        flashcards.append(
            {'topic': topic, 'question': question, 'answer': answer, 'hint': hint})

        sql_command = "INSERT INTO Questions(Topic, Question, Answer, Hint) VALUES (%s, %s, %s, %s)"
        val: Tuple[str, str, str, str] = (topic, question, answer, hint)
        crsr.execute(sql_command, val)
        mydb.commit()

        return render_template('create_fc.html', flashcard_count=len(flashcards) + 1, topic=topic)
    return render_template('create_fc.html', flashcard_count=1, topic=topic)


@app.route('/select_set')
def select_set() -> str:
    """
    Select a set of flashcards to use.
    Fetches all unique topics from the database to display as options.

    :return: Render select set template with available sets.
    """
    sql_command = "SELECT Topic FROM Questions GROUP BY Topic"
    crsr.execute(sql_command)
    myresult = crsr.fetchall()
    return render_template('select_set.html', sets=myresult)


@app.route('/fc_use/<int:index>', methods=['GET', 'POST'])
def fc_use(index: int) -> str:
    """
    Use the flashcards in a set.
    GET: Displays the flashcard at the given index.
    POST: Validates the user's answer and provides feedback.

    :param index: Index of the flashcard to display.
    :return: Render flashcard use template with the current flashcard and navigation options.
    """
    global topic
    sql_command = "SELECT * FROM Questions WHERE Topic = %s"
    val: List[str] = [topic]
    crsr.execute(sql_command, val)
    data = crsr.fetchall()

    if len(flashcards) == 0:
        for item in data:
            flashcards.append(
                {'topic': item[1], 'question': item[2], 'answer': item[3], 'hint': item[4]})

    if not flashcards:
        return redirect(url_for('create_fc'))

    flashcard: Dict[str, str] = flashcards[index]
    total_cards: int = len(flashcards)
    message: str = None

    if request.method == 'POST':
        user_answer: str = request.form['answer']
        if user_answer.lower() == flashcard['answer'].lower():
            message = "Correct!"
        else:
            message = "Wrong answer. Try again!"
            flashcards.append(flashcard)

    next_index: int = (index + 1) % total_cards
    prev_index: int = (index - 1 + total_cards) % total_cards

    return render_template('fc_use.html', flashcard=flashcard, current_index=index, next_index=next_index, prev_index=prev_index, total_cards=total_cards, message=message, topic=topic)


if __name__ == '__main__':
    # Starts the Flask application with debug mode enabled for live reloading and detailed error logs.
    # The application is bound to '0.0.0.0' allowing it to be accessible from any IP address, facilitating Docker container access.
    # It listens on port 5000, the default port for Flask applications.
    app.run(debug=True, host='0.0.0.0', port=5000)
